#include <algorithm>
#include <iterator>
#include <exception>
#include <boost/scoped_array.hpp>

#ifndef __STACK_HPP__
#define __STACK_HPP__

template <typename T> class Stack {
	public:
		Stack(size_t s);
		Stack(const Stack<T> &rhs);
		void operator= (const Stack<T> &rhs);
		~Stack();
		void push(const T &item);
		void pop(T &item);
		bool is_empty() const;
		bool is_full() const;
		void swap(Stack &t);
		void print();
	private:
		size_t top_, size_;
		boost::scoped_array<T> stack_;
};

#include "stack.tcc"

#endif /** __STACK_HPP__ **/
