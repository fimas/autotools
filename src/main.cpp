#include <iostream>
#include <exception>
#include "stack.hpp"

int main()
{
	try {
		Stack<int> mystack(10);
		mystack.push(1);
		mystack.push(2);
		mystack.push(3);
		mystack.push(4);
		mystack.push(5);
		mystack.push(6);
		mystack.push(7);
		mystack.push(8);
		mystack.push(9);
		mystack.push(10);

		mystack.print();
	}
	catch (std::bad_alloc &e) {
		std::cerr << "Memory exhausted!" << std::endl;
	}
	catch (std::out_of_range &e) {
		std::cerr << "No such index!" << std::endl;
	}
	catch (...) {
		std::cerr << "Unhandled exception!" << std::endl;
	}
}
