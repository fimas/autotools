template <typename T> inline
Stack<T>::Stack(size_t s)
  : top_(0), size_(s), stack_(new T[s]) { }

  template <typename T> inline
  Stack<T>::Stack(const Stack<T> &rhs)
  : top_(rhs.top_), size_(rhs.size_), stack_(0) {
    boost::scoped_array<T> tmp(new T[rhs.size_]);

    for (size_t i = 0; i < rhs.size_; ++i) {
      tmp.stack_[i] = rhs.stack_[i];
    }

    tmp.swap(stack_);
  }

template <typename T> inline void
Stack<T>::operator= (const Stack<T> &rhs) {
  if (this == &rhs)
    return;

  Stack tmp(rhs);
  swap(tmp);
}

template <typename T> inline
Stack<T>::~Stack() { 
  /* no-op! */
}

template <typename T> inline void
Stack<T>::push(const T &item) {
  if (is_full()) throw std::out_of_range("");
  stack_[top_++] = item;
}

template <typename T> inline void
Stack<T>::pop(T &item) {
  if (is_empty()) throw std::out_of_range("");
  item = stack_[--top_];
}

template <typename T> inline bool
Stack<T>::is_empty() const {
  return 0 == top_;
}

template <typename T> inline bool
Stack<T>::is_full() const {
  return top_ == size_;
}

template <typename T>inline void
Stack<T>::swap(Stack &t) {
  std::swap(t.top_, top_);
  std::swap(t.size_, size_);
  t.stack_.swap(stack_);
}

template <typename T>inline void
Stack<T>::print() {
  std::copy_n(stack_.get(), top_, std::ostream_iterator<T>(std::cout, " "));
}
